/**
 *	Twitter Hashtag js
 *
 **/
(function ($) {
	Drupal.behaviors.twitter_hashtag = {
		attach : function(context, settings) {
			if (!$('#twitter-hashtag-block').exists()){
				return false;
			}

			var hashtag = $('#twitter-hashtag-block').data('hashtag');
			var $tweets_container = $('#twitter-hashtag-block .tweets');
			var loading_tweets_animation = '<div class="twitter-bird-animation"></div>';

			loadtweets(hashtag, 'top');

			// Load more Tweets
			$('#twitter-hashtag-block .load-more').click( function() {
				var max_id = $(this).attr('data-maxid');

				if (max_id !== '') {
					$tweets_container.find('.last_tweet').removeClass('last_tweet');
					$tweets_container.append(loading_tweets_animation);
					loadtweets(hashtag, max_id);
				}
			});

			// Load all Tweets
			$('.load_all_tweets').live('click', function() {
				loadtweets(hashtag, 'top');
			});
		}
	};

	// Simple exists function
	$.fn.exists = function(){
		return this.length > 0 ? this : false;
	};

	// Load Tweets
	function loadtweets(hashtag, maxid) {
		$.ajax({
			type: 'GET',
			url: '/twitter_hashtag/'+ hashtag +'/'+ maxid,
			dataType: 'json',
			success: rendertweets
		});
	}

	// Render Tweets
	function rendertweets(value) {
		var $tweets_container = $('#twitter-hashtag-block .tweets');
		var $tweets_morelink  = $('#twitter-hashtag-block .load-more');
		var hashtag           = $('#twitter-hashtag-block').attr('data-hashtag');
		var hashtag_title     = $('#twitter-hashtag-block').attr('data-hashtag-title');
		var hashtag_search    = $('#twitter-hashtag-block').attr('data-hashtag-search');

		var stream_end_text   = '<div class="stream-end">'+
									'<i class="icon-twitter"></i>'+
									'<p>Du hast das Ende der Top Tweets für <strong>#'+ hashtag_title +'</strong> erreicht.</p>'+
									'<p><a class="" href="https://twitter.com/search/?q=%23'+ hashtag_search +'" target="_blank" >Alle Tweets anzeigen</a>.</p>'+
								'</div>';

		if (value !== '') {
			var tweets      = value['html'];
			var max_id      = value['tweet_max_id'];
			var search_type = value['max_id'];

			//-- Check for more Tweets
			if (search_type === 'all') {
				$tweets_container.html(tweets);
				$tweets_morelink.remove();
				$('#twitter-hashtag-block .stream-end').remove();
			}

			else {
				$('.twitter-bird-animation').remove();
				$tweets_container.append(tweets);

				if (max_id !== '') {
					$tweets_morelink.attr('data-maxid', max_id);
				}

				else {
					$tweets_morelink.remove();
					$('#twitter-hashtag-block').append(stream_end_text);
				}
			}
		}

		else {
			$tweets_morelink.remove();
			$('.twitter-bird-animation').remove();
			$('#twitter-hashtag-block').append(stream_end_text);
		}
	}
})(jQuery);
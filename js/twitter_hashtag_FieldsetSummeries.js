/**
 *  Twitter Hashtag FieldsetSummeries
 *
 */
(function ($) {
  Drupal.behaviors.twitterhashtagFieldsetSummeries = {
    attach: function (context) {
      $('fieldset.twitter-hashtag-form', context).drupalSetSummary(function (context) {
        var hashtag_value = $('#edit-twitter-hashtag-hashtag').val();
        if($.trim(hashtag_value)) {
          var hashtag_array = hashtag_value.split(',');
          var hashtag_summary = '';
          $.each(hashtag_array, function(index, value) {
            value = $.trim(value);
            if(value.length > 0) {
              hashtag_summary += (index != '0')? ', #' + value : '#' + value;
            }
          });
          return Drupal.t('Enable with ' + hashtag_summary);
        }
        else {
          return Drupal.t('Hashtag Block disabled');
        }
      });
    }
  };
})(jQuery);

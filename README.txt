Welcome to Twitter Hashtag Module.

Twitter Hashtag Module uses Twitter Search API to search Tweets with a Hashtag, and create a Block Element with all of this Tweets.

To use the Twitter Search API we use twitter-api-php[https://github.com/J7mbo/twitter-api-php] from J7mbo[https://github.com/J7mbo].

Installation:
=====================
1. Download the module and extract it in the folder sites/all/modules/contrib.
2. Go to the Module page at Administer > Modules and enable it. (http://[your_domain]/admin/modules)
3. Register your application with Twitter and add the provided keys here: http://[your_domain]/admin/config/services/twitter_hashtag/oauth.
4. Create a content type if it is necessary.
5. Connect the content type with the Twitter Hashtag Module here: http://[your_domain]/admin/config/services/twitter_hashtag.
6. Add ISO 639-1 code to restrict Tweets to the given language.
7. Create content of the said Content Type.
8. Write the hashtags in the special field.
9. Add Twitter Hashtag Block for Content Typ Output.

Done!
<?php
/**
 * Tweet User Template
 *
 */
$tweet_user_url         = $data['tweet_user_url'];
$tweet_user_foto        = $data['tweet_user_foto'];
$tweet_user_name        = $data['tweet_user_name'];
$tweet_user_screen_name = $data['tweet_user_screen_name'];
?>
<div class="header tweet-user">
	<a href="<?php print $tweet_user_url; ?>" class="tweet-user-url" target="_blank">
    	<img src="<?php print $tweet_user_foto; ?>" alt="" class="tweet-user-foto">
        <span class="tweet-user-name">
        	<span><?php print $tweet_user_name; ?></span>
        </span>
        <span class="tweet-user-screen-name">@<b><?php print $tweet_user_screen_name; ?></b></span>
    </a>
</div>
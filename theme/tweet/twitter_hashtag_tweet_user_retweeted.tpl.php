<?php
/**
 * Tweet User[Retweeted] Template
 *
 */
$retweeted_user_name        = $data['retweeted_user_name'];
$retweeted_user_screen_name = $data['retweeted_user_screen_name'];
$retweeted_user_foto        = $data['retweeted_user_foto'];
$retweeted_user_url         = 'https://twitter.com/' . $tweet_user_screen_name;
?>
<div class="header tweet-user">
	<a href="<?php print $retweeted_user_url; ?>" class="tweet-user-url" target="_blank">
    	<img src="<?php print $retweeted_user_foto; ?>" alt="" class="tweet-user-foto">
        <span class="tweet-user-name">
        	<span><?php print $retweeted_user_name; ?></span>
		</span>
        <span class="tweet-user-screen-name">@<b><?php print $retweeted_user_screen_name; ?></b></span>
    </a>
</div>
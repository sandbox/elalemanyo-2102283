<?php
/**
 * Tweet Text Template
 *
 */
$tweet_text             = $data['tweet_text'];
$tweet_user_name        = $data['tweet_user_name'];
$tweet_user_url         = $data['tweet_user_url'];
$tweet_user_screen_name = $data['tweet_user_screen_name'];

$text_replaces['links'] = array(
	'pattern' => '/((http)+(s)?:\/\/[^<>\s]+)/i',
	'replacement' => '<a href=\"\\0\" target=\"_blank\">\\0</a>'
);

$text_replaces['user'] = array(
	'pattern' => '/[@]+([A-Za-z0-9-_]+)/',
	'replacement' => '<a href=\"http://twitter.com/\\1\" target=\"_blank\">\\0</a>'
);

$text_replaces['search'] = array(
	'pattern' => '/#(\w*[a-zA-ZzÄäÖöÜüÀÁáíÂâÈèÉéÊêóÙùÚúßÇç_+-]+\w*)/',
	'replacement' => '<a href=\"http://twitter.com/search?q=%23\\1\" target=\"_blank\">\\0</a>'
);

foreach ($text_replaces as $text_replace) {
	$tweet_text = preg_replace($text_replace['pattern'], $text_replace['replacement'], $tweet_text);
}
?>
<div class="tweet-content">
	<p class="tweet-text"><?php echo $tweet_text?></p>

	<?php if ($data['retweeted_status']): ?>
	<div class="retweet-credit">
		<i class="icon-retweet">r</i>
        <?php print t('Retweeted by '); ?>
        <a title="<?php print t('@@tweet_user_name on Twitter', array('@tweet_user_name' => $tweet_user_name)); ?>" href="<?php print $tweet_user_url; ?>" class="user" target="_blank">
			<?php print $tweet_user_screen_name; ?>
		</a>
	</div>
    <?php endif; ?>
</div>

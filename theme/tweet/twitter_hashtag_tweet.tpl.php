<?php
/**
 *	Tweets Template
 *  tweet html:
 *  <li class="tweet">
 *	 <a data-datetime="" href="" class="permalink" target="_blank"><time title="" datetime="" class="dt-updated"></time></a>
 *	 <div class="header tweet-user">
 *  	<a href="https://twitter.com/Juliio_Felix" class="tweet-user-url" target="_blank">
 *   		<img src="" alt="" class="tweet-user-foto">
 *      	<span class="tweet-user-name"><span></span></span>
 *		    <span class="tweet-user-screen-name">@<b></b></span>
 *      </a>
 *   </div>
 *   <div class="tweet-content">
 *   	<p class="tweet-text">
 *    		<a href="" target="_blank"></a>
 *    		<a href="" target="_blank"></a>
 *    	</p>
 *   </div>
 *   <div class="footer customisable-border">
 *   	<ul class="tweet-actions">
 *	    	<li><a title="Reply" class="reply-action web-intent" href="" target="_blank"><i class="icon-reply"></i><b>Reply</b></a></li>
 *	        <li><a title="Retweet" class="retweet-action web-intent" href="" target="_blank"><i class="icon-retweet"></i><b>Retweet</b></a></li>
 *	        <li><a title="Favourite" class="favorite-action web-intent" href="" target="_blank"><i class="icon-favorit"></i><b>Favourite</b></a></li>
 *   	</ul>
 *	 </div>
 *  </li>
 *
 */
$tweets = $data['tweets'];
$tweet_max_id = '';

if(isset($tweets['search_metadata']['next_results'])) {
	$next_tweet_url = parse_url($tweets['search_metadata']['next_results']);
    parse_str($next_tweet_url['query'], $query);
    $tweet_max_id   = $query['max_id'];
}

//-- Render Tweets
$tweets           = $tweets['statuses'];
$tweet_block_html = '';
$numtweets = count($tweets);
$i = 0;

foreach($tweets as $tweet) {
    $special_class = (++$i === $numtweets)? 'last_tweet' : '';
    $tweet_id      = $tweet['id_str'];

    //-- Tweet User info
    $tweet_user_name        = $tweet['user']['name'];
    $tweet_user_screen_name = $tweet['user']['screen_name'];
    $tweet_user_url         = 'https://twitter.com/' . $tweet_user_screen_name;

    $tweet_block_html .= '<li class="tweet '. $special_class .'">';

	//-- Tweet time
    $tweet_time_variables = array(
		'time'             => strtotime($tweet['created_at']),
		'tweet_created_at' => $tweet['created_at'],
		'tweet_direct_url' => 'https://twitter.com/'. $tweet_user_name .'/status/'. $tweet_id,
	);
	$tweet_block_html .= theme('twitter_hashtag_tweet_time', array('data' => $tweet_time_variables));

    //-- Tweet User
    if (isset($tweet['retweeted_status'])) {
    	$twitter_hashtag_tweet_user_retweeted_variables = array(
			'retweeted_user_name'        => $tweet['retweeted_status']['user']['name'],
			'retweeted_user_screen_name' => $tweet['retweeted_status']['user']['screen_name'],
			'retweeted_user_foto'        => $tweet['retweeted_status']['user']['profile_image_url'],
		);
		$tweet_block_html .= theme('twitter_hashtag_tweet_user_retweeted', array('data' => $twitter_hashtag_tweet_user_retweeted_variables));
    }

    else {
		$tweet_user_variables = array(
			'tweet_user_url'         => $tweet_user_url,
			'tweet_user_foto'        => $tweet['user']['profile_image_url'],
			'tweet_user_name'        => $tweet_user_name,
			'tweet_user_screen_name' => $tweet_user_screen_name,
		);
		$tweet_block_html .= theme('twitter_hashtag_tweet_user', array('data' => $tweet_user_variables));
    }

    //-- Tweet Content
    $tweet_text = (!isset($tweet['retweeted_status']))? $tweet['text'] : $tweet['retweeted_status']['text'];
    $twitter_hashtag_tweet_text_variables = array(
        'tweet_text'             => $tweet_text,
        'retweeted_status'       => isset($tweet['retweeted_status'])? TRUE : FALSE,
        'tweet_user_name'        => $tweet_user_name,
        'tweet_user_url'         => $tweet_user_url,
        'tweet_user_screen_name' => $tweet_user_screen_name
    );
    $tweet_block_html .= theme('twitter_hashtag_tweet_text', array('data' => $twitter_hashtag_tweet_text_variables));

    //-- Tweet Footer
	$tweet_block_html .= theme('twitter_hashtag_tweet_footer', array('tweet_id' => $tweet_id));

    $tweet_block_html .= '</li>';
}

$json = array(
    'html' => $tweet_block_html,
    'tweet_max_id' => $tweet_max_id,
    'max_id' => $max_id
);
drupal_json_output($json);

<?php
/**
 * Tweet Time Template
 *
 */
$tweet_created_at = $data['tweet_created_at'];
$tweet_direct_url = $data['tweet_direct_url'];
$time = $data['time'];
$h_time = sprintf(('%s'), _twitter_hashtag_human_time_diff($time));
$tweet_created_at_beauty = t('Posted @time', array('@time' => date('j F, Y, H:i:s', $time)));
?>
<a data-datetime="<?php print $tweet_created_at; ?>" href="<?php print $tweet_direct_url; ?>" class="permalink" target="_blank">
	<time title="<?php print $tweet_created_at_beauty; ?>" datetime="<?php print $tweet_created_at; ?>" class="dt-updated">
		<?php print $h_time; ?>
	</time>
</a>

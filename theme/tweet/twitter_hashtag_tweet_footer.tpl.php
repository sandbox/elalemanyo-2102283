<?php
/**
 * Tweet Footer Template
 *
 */
$replay_url   = 'https://twitter.com/intent/tweet?in_reply_to='. $tweet_id;
$retweet_url  = 'https://twitter.com/intent/retweet?tweet_id='. $tweet_id;
$favorite_url = 'https://twitter.com/intent/favorite?tweet_id='. $tweet_id;
?>
<div class="twitter-hashtag-footer customisable-border">
    <ul class="tweet-actions">
        <li>
            <a title="<?php print t('Reply'); ?>" class="reply-action" href="<?php print $replay_url; ?>" target="_blank">
                <i class="icon-reply">a</i><b><?php print t('Reply'); ?></b>
            </a>
        </li>
        <li>
            <a title="<?php print t('Retweet'); ?>" class="retweet-action" href="<?php print $retweet_url; ?>" target="_blank">
                <i class="icon-retweet">r</i><b><?php print t('Retweet'); ?></b>
            </a>
        </li>
        <li>
            <a title="<?php print t('Favourite'); ?>" class="favorite-action" href="<?php print $favorite_url; ?>" target="_blank">
            	<i class="icon-favorit">f</i><b><?php print t('Favourite'); ?></b>
            </a>
        </li>
    </ul>
</div>
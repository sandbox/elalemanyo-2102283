<?php
/**
 * Twitter Hashtag Help Page
 *
 */
?>
<h3>Twitter Hashtag Module</h3>
<div>
	<h4><b>Installation:</b></h4>
	<ol>
		<li>Download the module and extract it in the folder sites/all/modules/contrib.</li>
		<li>Go to the Module page at Administer > Modules and enable it.</li>
		<li><a href="https://dev.twitter.com/apps/new" target="_blank">Register your application</a> with Twitter and add the provided keys <a href="/admin/config/services/twitter_hashtag/oauth" target="_blank">here</a>.</li>
		<li>Create a content type if it is necessary.</li>
		<li>Connect the content type with the Twitter Hashtag Module <a href="/admin/config/services/twitter_hashtag" target="_blank">here</a>.</li>
		<li>Add <a href="http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes" target="_blank">ISO 639-1 code</a> to restrict Tweets to the given language.</li>
		<li>Create content of the said Content Type.</li>
		<li>Write the hashtags in the special field.</li>
		<li>Add Twitter Hashtag Block for Content Typ Output.</li>
		<b>Done!</b>
	</ol>
</div>
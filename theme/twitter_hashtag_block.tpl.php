<?php
/**
 *  Simple Template for the Tweets Block
 *
 */
?>
<?php if (!empty($content)):?>
	<?php
	$hashtag_title  = str_replace(',',', #', $content);
	$hashtag_search = str_replace(',', '%20OR%20%23', $content);
	?>

	<div id="twitter-hashtag-block" data-hashtag="<?php print $content; ?>" data-hashtag-title="<?php print $hashtag_title; ?>" data-hashtag-search="<?php print $hashtag_search; ?>">
		<div class="timeline-header">
			<span class="summary">
  				<a title="<?php print t('Search #@hashtag on Twitter', array('@hashtag' => $hashtag_title)); ?>" href="https://twitter.com/search/?q=%23<?php print $hashtag_search; ?>" target="_blank">
  					#<?php print $hashtag_title; ?>
  				</a>
			</span>
			<a title="Twitter" href="https://twitter.com" class="icon-twitter-link" target="_blank"><i class="icon-twitter">t</i><b>Twitter</b></a>
		</div>
		<ul class="tweets">
			<div class="twitter-bird-animation"></div>
		</ul>
		<button class="load-more" data-maxid=""><?php print t('Load more'); ?></button>
	</div>
<?php endif ?>

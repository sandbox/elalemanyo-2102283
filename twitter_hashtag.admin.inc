<?php
/**
 * Twitter Hashtag Settings
 *
 */
function twitter_hashtag_settings() {
	$form = array();

	$form['twitter_hashtag_types'] = array(
		'#title'         => t('Node types'),
		'#type'          => 'select',
		'#multiple'      => TRUE,
		'#description'   => t('Choose which node types should support Twitter Hashtag Block.'),
		'#options'       => node_type_get_names(),
		'#default_value' => variable_get('twitter_hashtag_types', NULL),
  	);

  	$form['twitter_hashtag_lang'] = array(
		'#title'         => t('Language'),
		'#type'          => 'textfield',
		'#description'   => t('Restricts tweets to the given language, given by an <a href="http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes" target="_blank">ISO 639-1 code</a>.'),
		'#default_value' => variable_get('twitter_hashtag_lang', NULL),
  	);

  	return system_settings_form($form);
};

/**
 * Twitter Hashtag Settings Oauth
 *
 */
function twitter_hashtag_settings_oauth() {
	$form = array();
	$form['twitter_hashtag_oauth'] = array(
		'#type'        => 'fieldset',
		'#title'       => t('OAuth Settings'),
		'#description' => t('To enable OAuth based access for twitter, you must <a href="@url">register your application</a> with Twitter and add the provided keys here.', array('@url'=> 'https://dev.twitter.com/apps/new')),
	);
	$form['twitter_hashtag_oauth']['twitter_hashtag_callback_url'] = array(
		'#type'   => 'item',
		'#title'  => t('Callback URL'),
		'#markup' => url('twitter/oauth', array('absolute' => TRUE)),
	);
	$form['twitter_hashtag_oauth']['twitter_hashtag_consumer_key'] = array(
		'#type'          => 'textfield',
		'#title'         => t('OAuth Consumer key'),
		'#default_value' => variable_get('twitter_hashtag_consumer_key', NULL),
	);
	$form['twitter_hashtag_oauth']['twitter_hashtag_consumer_secret'] = array(
		'#type'          => 'textfield',
		'#title'         => t('OAuth Consumer secret'),
		'#default_value' => variable_get('twitter_hashtag_consumer_secret', NULL),
	);
	$form['twitter_hashtag_oauth']['twitter_hashtag_oauth_access_token'] = array(
		'#type'          => 'textfield',
		'#title'         => t('OAuth Access Token'),
		'#default_value' => variable_get('twitter_hashtag_oauth_access_token', NULL),
	);
	$form['twitter_hashtag_oauth']['twitter_hashtag_oauth_access_token_secret'] = array(
		'#type'          => 'textfield',
		'#title'         => t('OAuth Access Token Secret'),
		'#default_value' => variable_get('twitter_hashtag_oauth_access_token_secret', NULL),
	);

  	return system_settings_form($form);
};